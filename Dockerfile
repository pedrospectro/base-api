FROM ruby:2.3.3-alpine
RUN apk --update add --virtual build-dependencies ruby-dev build-base mariadb-dev
RUN mkdir /base-api
WORKDIR /base-api
COPY Gemfile /base-api/Gemfile
COPY Gemfile.lock /base-api/Gemfile.lock
RUN bundle install
COPY . /base-api
COPY config/database.dev.yml /base-api/config/database.yml
EXPOSE 80
CMD rails server -b 0.0.0.0 -p 80