FactoryGirl.define do
  factory :payment do
    trait :pagseguro_payment do
      provider "pagseguro"
      transaction_code ""
      transaction_date "2018-02-17 20:22:22"
      status "ready"
      compensacao_date "2018-02-17"
      taxa_intermediacao 4
      payment_method "credit card"
      pedido_id 423
    end

    trait :another_pagseguro_payment do
      provider "pagseguro"
      transaction_code ""
      transaction_date "2018-02-17 20:22:22"
      status "ready"
      compensacao_date "2018-02-17"
      taxa_intermediacao 4
      payment_method "credit card"
      pedido_id 425
    end

    trait :new_valid_payment do
      provider "pagseguro"
      status "ready"
      pedido_id 427
    end

    trait :paid_payment do
      provider "pagseguro"
      status "paid"
      pedido_id 428
    end

    factory :pagseguro_payment, traits: [:pagseguro_payment]
    factory :another_pagseguro_payment, traits: [:another_pagseguro_payment]
    factory :new_valid_payment, traits: [:new_valid_payment]
    factory :invalid_payment, traits: [:invalid_payment_without_pedido]
    factory :paid_payment, traits: [:paid_payment]
  end
end
