FactoryGirl.define do
  factory :user do
    trait :valid do
      name 'The Name'
      email 'email@test.com.api'
      password 'password'
    end

    trait :forget do
      name 'The forget'
      email 'forget@test.com.api'
      password 'password'
    end

    trait :ptbr do
      name 'The PTBR'
      email 'PTBR@test.com.api'
      password Digest::SHA1.hexdigest('password')
      locale_string 'pt-BR'
    end

    trait :en do
      name 'The EN'
      email 'EN@test.com.api'
      password Digest::SHA1.hexdigest('password')
      locale_string 'en'
    end

    trait :pedido do
      id 420
      name 'Pedido user'
      email 'pedido_user@test.com.api'
      password 'password'
    end

    trait :item_controller do
      id 421
      name 'Pedido Item user'
      email 'pedido_item_user@test.com.api'
      password 'password'
    end

    trait :another_item_controller do
      id 422
      name 'Pedido another Item user'
      email 'pedidoanother_item_user@test.com.api'
      password 'password'
    end

    trait :payment_user do
      id 423
      name 'Payment user'
      email 'payment_user@test.com.api'
      password 'password'
    end

    trait :another_payment_user do
      id 424
      name 'Another Payment user'
      email 'another_payment_user@test.com.api'
      password 'password'
    end

    trait :admin_user do
      id 425
      name 'New admin user'
      email 'new_admin_user@test.com.api'
      password 'password'
    end

    trait :invalid do
    end
    factory :valid_user, traits: [:valid]
    factory :pedido_user, traits: [:pedido]
    factory :item_user, traits: [:item_controller]
    factory :another_item_user, traits: [:another_item_controller]
    factory :forget_user, traits: [:forget]
    factory :invalid_user, traits: [:invalid]
    factory :ptbr_user, traits: [:ptbr]
    factory :en_user, traits: [:en]
    factory :payment_user, traits: [:payment_user]
    factory :another_payment_user, traits: [:another_payment_user]
    factory :admin_user, traits: [:admin_user]
  end
end
