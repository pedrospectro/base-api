FactoryGirl.define do
  factory :item do
    trait :valid do
      quantity 2
      unity_value 1.5
      deleted_at nil
    end

    trait :created do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 421
    end

    trait :another do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 422
    end

    trait :payment do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 423
    end

    trait :without_payment do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 424
    end

    trait :another_payment do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 425
    end

    trait :without_another_payment do
      quantity 3
      unity_value 1.5
      deleted_at nil
      pedido_id 426
    end

    factory :valid_item, traits: [:valid]
    factory :created_item, traits: [:created]
    factory :another_item, traits: [:another]
    factory :payment_item, traits: [:payment]
    factory :another_payment_item, traits: [:another_payment]
    factory :without_payment_item, traits: [:without_payment]
    factory :without_another_payment_item, traits: [:without_another_payment]
  end
end
