FactoryGirl.define do
  factory :pedido do
    trait :valid do
      id 420
      user_id 420
    end

    trait :ready do
      id 421
      user_id 421
    end

    trait :new_ready do
      id 555
    end

    trait :another do
      id 422
      user_id 422
    end

    trait :payment do
      id 423
      user_id 423
    end

    trait :without_payment do
      id 424
      user_id 423
    end

    trait :another_payment do
      id 425
      user_id 424
    end

    trait :without_another_payment do
      id 426
      user_id 424
    end

    trait :with_another_payment_mock do
      id 427
      user_id 424
    end

    trait :paid_pedido do
      id 428
      user_id 424
    end

    factory :paid_pedido, traits: [:paid_pedido]
    factory :new_ready, traits: [:new_ready]
    factory :valid_pedido, traits: [:valid]
    factory :ready_pedido, traits: [:ready]
    factory :another_pedido, traits: [:another]
    factory :payment_pedido, traits: [:payment]
    factory :another_payment_pedido, traits: [:another_payment]
    factory :without_payment_pedido, traits: [:without_payment]
    factory :without_another_payment_pedido, traits: [:without_another_payment]
    factory :with_another_payment_mock, traits: [:with_another_payment_mock]
  end
end
