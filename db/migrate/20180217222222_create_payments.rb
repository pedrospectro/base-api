class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.string :provider
      t.string :transaction_code
      t.timestamp :transaction_date
      t.string :status
      t.date :compensacao_date
      t.float :taxa_intermediacao
      t.string :payment_method
      t.references :pedido

      t.timestamps
    end
  end
end
