class CreateUserLocationReference < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :location, foreign_key: false
  end
end
