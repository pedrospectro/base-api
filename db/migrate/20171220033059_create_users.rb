class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.datetime :dob
      t.datetime :deleted_at
      t.string :api_key
      t.string :cpf

      t.timestamps
    end
  end
end
