class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.float :value
      t.string :warranty
      t.string :category
      t.integer :quantity
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
