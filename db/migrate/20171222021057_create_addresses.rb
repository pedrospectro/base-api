class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :postal_code
      t.string :country
      t.string :uf
      t.string :city
      t.string :neigborhood
      t.string :street
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
