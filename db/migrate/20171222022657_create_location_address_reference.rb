class CreateLocationAddressReference < ActiveRecord::Migration[5.1]
  def change
    add_reference :locations, :address, foreign_key: true
  end
end
