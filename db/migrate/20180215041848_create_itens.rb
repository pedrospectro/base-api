class CreateItens < ActiveRecord::Migration[5.1]
  def change
    create_table :itens do |t|
      t.integer :quantity
      t.float :unity_value
      t.float :total_value
      t.references :pedido
      t.timestamp :deleted_at
      t.timestamps
    end
  end
end
