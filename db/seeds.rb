@role_user = Role.create(
  name: 'user',
  description: 'user'
)

@role_admin = Role.create(
  name: 'admin',
  description: 'admin'
)

@role_seller = Role.create(
  name: 'seller',
  description: 'seller'
)

@address = Address.create(
  postal_code: '68743010',
  country: 'Brasil',
  uf: 'PA',
  city: 'Castanhal',
  neigborhood: 'Centro',
  street: 'Quintino bocaiuva'
)

@location = Location.create(
  address_id: @address.id,
  lat: 1.0,
  lng: -48.0,
  street_number: '1',
  km: '0'
)

@user = User.create(
  name: 'validName',
  email: 'valid_user@email.com',
  password:  '0d3be14154cb1f95f7b2d8f8879af4ef0a3fd9f4',
  dob: '2017-12-20 01:30:59',
  api_key: 'MyStringValid',
  cpf: 'MyString',
  location_id: @location.id
)

@association = RolesUser.create(
  user: @user,
  role: @role_user
)

@association_admin = RolesUser.create(
  user: @user,
  role: @role_admin
)

@user.products.store(
  name: 'one',
  value: 1.5,
  warranty: '1 Mes',
  category: 'one',
  quantity: 10
)

@user.products.store(
  name: 'two',
  value: 10,
  warranty: '2 Meses',
  category: 'two',
  quantity: 1
)

@user.pedidos.store(
  total_value: 100.49,
  status: 'ready'
)

@user.pedidos.store(
  total_value: 200,
  status: 'ready'
)
