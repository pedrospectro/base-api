Rails.application.routes.draw do
  get '/', to: 'application#info'
  # /API/V1/ ROUTES
  namespace :api  do
    namespace :v1 do
      # PUBLIC LOGIN SIGNUP SEARCH and FORGET/CHANGE PASSWORD
      post 'search', to: 'public#search'
      post 'login', to: 'public#login'
      post 'signup', to: 'public#signup'
      post 'forget', to: 'public#forget'
      post 'change_password/:api_key', to: 'public#change_password'
      get  'change_password/:api_key', to: 'public#change_password_page'

      # Products/itens RESOURCE
      resources :products, only: [:show, :index]

      # Admin RESOURCES
      namespace :admin do
        resources :roles

        # Admin Users
        scope module: 'users' do
          resources :users do
            resources :role_users, only: [:index, :create, :destroy]
            resources :addresses, only: [:index, :create]
            resources :pedidos, only: [:index, :show, :destroy, :update] do
              resources :itens, only: [:index, :show]
              resources :payments, only: [:index]
            end
          end
        end

        # Admin Sellers
        scope module: 'sellers' do
          resources :sellers do
            resources :products
          end
        end
      end

      # User RESOURCES
      scope module: 'users' do
        resources :users, only: [:show, :update, :destroy] do
          resources :role_users, only: [:index, :create, :destroy]
          resources :addresses, only: [:index, :create]
          resources :pedidos, only: [:index, :show, :create, :destroy] do
            resources :itens, only: [:show, :index]
            resources :payments, only: [:create, :index]
          end
        end
      end

      # Seller RESOURCES
      scope module: 'sellers' do
        resources :sellers, only: [:show] do
          resources :products do
            resources :reports, only: [:index]
          end
        end
      end

      # Pagseguro Payment Notifications Resource
      scope module: 'pagseguro' do
        resources :notifications, only: [:create]
      end
    end
  end
end
