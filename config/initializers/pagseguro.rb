PagSeguro.configure do |config|
  config.token = ENV['PAGSEGURO_API_KEY']
  config.email = ENV['PAGSEGURO_EMAIL']
end
if Rails.env.production? 
  PagSeguro.configuration.environment = :production
else
  PagSeguro.configuration.environment = :sandbox
end
