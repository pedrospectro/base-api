require 'correios-cep'
class GetAddressByCepJob < ApplicationJob
  def perform(cep)
    @address = Correios::CEP::AddressFinder.get(cep)
    {
      country: "Brasil",
      uf: @address[:state],
      city: @address[:city],
      neighborhood: @address[:neighborhood],
      street: @address[:address]
    }
  end
end
