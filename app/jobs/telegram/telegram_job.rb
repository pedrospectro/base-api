module Telegram
  require 'telegram/bot'
  class TelegramJob < ApplicationJob
    def bot_id
      Telegram::Bot::Client.new(ENV['TELEGRAM_BOT_ID'])
    end

    def chat_id
      ENV['TELEGRAM_CHAT_ID']
    end

    def send_msg(msg)
      @client = bot_id
      @client.send_message(chat_id: chat_id, text: msg)
    end

    def canceled_payment_msg(user, pedido, pagamento)
      I18n.t('telegram.canceled', user: user, pedido: pedido, pagamento: pagamento)
    end

    def payment_msg(user, pedido, pagamento)
      I18n.t('telegram.paid', user: user, pedido: pedido, pagamento: pagamento)
    end
  end
end
