module Telegram
  class SendTelegramMsgJob < Telegram::TelegramJob
    def perform(msg)
      is_to_send = Rails.env.production? || Rails.env.development?
      send_msg(msg) if is_to_send
    end
  end
end
