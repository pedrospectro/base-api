require 'erb'
require 'sendgrid-ruby'

module Email
  class EmailJob < ApplicationJob
    include SendGrid

    def generate_html(email_filename, binding_model)
      erb_file = "app/views/layouts/email/" + email_filename
      erb_str = File.read(erb_file)
      renderer = ERB.new(erb_str)
      renderer.result(binding_model.generate_binding).gsub(/"/, '\"').squish
    end

    def generate_email_data(to, subject, html)
      JSON.parse(
        '{
          "personalizations": [{
            "to": [{"email": "%s"}],
            "subject": "%s"
          }],
          "from": {"email": "%s"},
          "content": [{"type": "text/html","value": "%s"}]
        }' % [to, subject, 'test@example.com', html]
      )
    end

    def send_email(data)
      return true if Rails.env.test?
      sg = SendGrid::API.new(api_key: ENV['SENDGRID_API'])
      response = sg.client.mail._("send").post(request_body: data)
      return true if response.status_code == '202'
      return false if response.status_code != '202'
    end
  end
end
