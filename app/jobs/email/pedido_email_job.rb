module Email
  class PedidoEmailJob < EmailJob
    def perform(pedido)
      return if pedido.nil?
      @user = pedido.user
      @pedido = pedido
      @binding = Binding::PedidoEmailBinding.new(@user, @pedido)
      html_data = generate_html('pedido_email.html.erb', @binding)
      data = generate_email_data(
        @user.email,
        I18n.t('email.pedido_email.subject'),
        html_data
      )
      send_email(data)
    end
  end
end
