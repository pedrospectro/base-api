module Email
  class PedidoCanceledEmailJob < EmailJob
    def perform(pedido)
      return if pedido.nil?

      @pedido = pedido
      @user = @pedido.user
      @item_list = @pedido.items

      @binding = Binding::PaymentNotificationBinding.new(@user, @pedido, @item_list)
      html_data = generate_html('pedido_canceled_email.html.erb', @binding)

      data = generate_email_data(
        @user.email,
        I18n.t('email.pedido_canceled_email.subject'),
        html_data
      )
      send_email(data)
    end
  end
end
