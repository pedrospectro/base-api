module Email
  class ForgetPasswordEmailJob < EmailJob
    def perform(user)
      @user = user
      return if @user.nil?
      @user.update(
        api_key: user.generate_api_key,
        password: user.generate_api_key
      )
      @binding = Binding::SignupEmailBinding.new(@user)
      html_data = generate_html('forget_password_email.html.erb', @binding)
      data = generate_email_data(
        @user.email,
        I18n.t('email.forget_password_email.subject'),
        html_data
      )
      send_email(data)
    end
  end
end
