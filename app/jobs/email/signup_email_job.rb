module Email
  class SignupEmailJob < EmailJob
    def perform(user)
      @user = user
      return if @user.nil?
      @binding = Binding::SignupEmailBinding.new(@user)
      html_data = generate_html('signup_email.html.erb', @binding)
      data = generate_email_data(
        @user.email,
        I18n.t('email.signup_email.subject'),
        html_data
      )
      send_email(data)
    end
  end
end
