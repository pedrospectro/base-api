require 'geokit'

class GetLocationByAddressJob < ApplicationJob
  include Geokit::Geocoders
  def perform(address, number)
    @location_name = "%s %s %s %s %s %s" % [
      address.country,
      address.uf,
      address.city,
      address.neigborhood,
      address.street,
      number
    ]
    @coords = MultiGeocoder.geocode(@location_name)
    # @address_data = MultiGeocoder.reverse_geocode(@location_lat_lng_datas)
    {
      lat: @coords.lat,
      lng: @coords.lng,
      number: number
    }
    # @location = @address.locations.create(
    #   lat: @coords[:lat],
    #   lng: @coords[:lng],
    #   street_number: @coords[:number]
    # )
  end
end
