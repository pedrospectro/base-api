module Pagseguro
  class ProcessCheckoutJob < Pagseguro::PagseguroJob
    def perform(pedido)
      payment = PagSeguro::PaymentRequest.new
      payment.reference = pedido.id
      payment.notification_url = ''
      payment.redirect_url = ''

      for pedido_item in pedido.items
        payment.items << {
          id: pedido_item.id,
          description: 'product',
          amount: pedido_item.unity_value,
          weight: pedido_item.quantity,
          quantity: pedido_item.quantity
        }
      end

      response = payment.register
      response
    end
  end
end
