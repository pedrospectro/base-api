module Pagseguro
  class PagseguroJob < ApplicationJob
    def payment_method(type_id)
      type_id = type_id.to_i
      case type_id
      when 1
        return 'cartao credito'
      when 2
        return 'boleto'
      when 3
        return 'debito online'
      when 4
        return 'saldo pagseguro'
      when 5
        return 'oi celular'
      when 7
        return 'deposito em conta'
      else
        return 'nao definido'
      end
    end
  end
end
