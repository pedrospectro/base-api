module Pagseguro
  class ProcessNotificationJob < Pagseguro::PagseguroJob
    def perform(transaction, status)
      @pedido = Pedido.find(transaction.reference.to_i)
      @pedido.update(status: status) if status != "disponivel"
      @pagamento = @pedido.payment

      @pagamento.update(
        payment_method: payment_method(transaction.payment_method.type_id),
        status: status,
        transaction_code: transaction.code,
        transaction_date: transaction.created_at,
        compensacao_date: transaction.escrow_end_date,
        taxa_intermediacao: (
          transaction.creditor_fees.intermediation_fee_amount +
          transaction.creditor_fees.intermediation_rate_amount
        )
      )
      @pagamento
    end
  end
end
