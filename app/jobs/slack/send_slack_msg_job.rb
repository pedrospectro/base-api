require 'slack-notifier'

module Slack
  class SendSlackMsgJob < ApplicationJob
    def perform(msg, emoji)
      notifier = Slack::Notifier.new(
        ENV['SLACK_URL'],
        channel: ENV['SLACK_CHANNEL'],
        username: 'Api-bot'
      )

      is_to_send = Rails.env.production? || Rails.env.development?
      notifier.ping(emoji + msg) if is_to_send
    end
  end
end
