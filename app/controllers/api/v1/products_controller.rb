module Api
  module V1
    class ProductsController < Api::V1::PublicController
      before_action :product, only: [:show]

      def index
        render json: page_index(Product, params)
      end

      def show
        render json: @product
      end

      private

      def product
        @product = Product.where(id: params[:id], deleted_at: nil).first
        render status: :not_found if @product.nil?
      end
    end
  end
end
