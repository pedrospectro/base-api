module Api
  module V1
    module Pagseguro
      class PagseguroController < Api::V1::PublicController
        private

        def find_transaction(notification_code)
          PagSeguro::Transaction.find_by_notification_code(notification_code)
        end

        def status_info(status)
          status = I18n.t('msg.notification.boleto') if status == 'aguardando'
          status
        end

        def msg_info(pedido, status, user, payment_method)
          I18n.t(
            'telegram.payment_notification',
            pedido_id: pedido.id.to_s,
            status: status,
            user_name: user.name,
            total_value: pedido.total_value.to_s,
            payment_method: payment_method
          )
        end

        def status_transaction(id)
          status = "aguardando" if id == 1
          status = "analise" if id == 2
          status = "pago" if id == 3
          status = "disponivel" if id == 4
          status = "disputa" if id == 5
          status = "devolvida" if id == 6
          status = "cancelada" if id == 7
          status = "debitado" if id == 8
          status = "retencao" if id == 9
          status
        end

        def send_msg(status, pedido)
          @status = status.to_s
          @pedido = pedido
          @valor_total = @pedido.total_value
          @item_list = @pedido.items
          @user = @pedido.user
          @payment_method = @pedido.payment.payment_method

          paid = @status == 'pago'
          canceled = (@status == "cancelada" || @status == 'devolvida')
          Email::PedidoPaidEmailJob.perform_now(@pedido) if paid
          Email::PedidoCanceledEmailJob.perform_now(@pedido) if canceled

          @status = status_info(@status)
          msg = msg_info(@pedido, @status, @user, @payment_method)
          emoji = I18n.t('slack_emoji.pagseguro')
          Slack::SendSlackMsgJob.perform_now(msg, emoji) if @status != "disponivel"
          Telegram::SendTelegramMsgJob.perform_now(msg) if @status != "disponivel"
        end
      end
    end
  end
end
