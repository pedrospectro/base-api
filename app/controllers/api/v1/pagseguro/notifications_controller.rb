module Api
  module V1
    module Pagseguro
      class NotificationsController < Api::V1::Pagseguro::PagseguroController
        def create
          transaction = find_transaction(params[:notificationCode])

          if transaction.errors.empty?
            status = status_transaction(transaction.status.id.to_i)
            @pedido = Pedido.find(transaction.reference.to_i)

            if @pedido.status != status
              @pedido.process_notification(transaction, status)
              send_msg(status, @pedido)
            end
          end
          head :ok
        end
      end
    end
  end
end
