module Api
  module V1
    class PublicController < ApplicationController
      include PublicControllerHelper

      before_action :user_by_api_key, only: [
        :change_password,
        :change_password_page
      ]

      def search
        @json = JSON.parse(request.body.read)
        if @json['search'].nil? || @json["search"] == ''
          render json: []
        else
          render json: Product.where("(name LIKE '%s') or (category LIKE '%s')" % [
            "%" + @json['search'] + "%",
            "%" + @json['search'] + "%"
          ])
        end
      end

      def login
        @json['password'] = '' if @json['password'].nil?
        @user = User.where(
          email: @json["email"],
          password: Digest::SHA1.hexdigest(@json['password']),
          deleted_at: nil
        )
        if @user != []
          locale_on_login(@user.first)
          render status: :ok, json: @user.first
        else
          render status: :not_found, json: {
            msg: I18n.t('msg.public.invalid_user_password')
          }
        end
      end

      def signup
        @user = User.create(@json)
        if @user.valid?
          @user.update(password: Digest::SHA1.hexdigest(@json['password']))
          RolesUser.create(user: @user, role: Role.first)
          render status: :created, json: @user
        else
          render status: :unprocessable_entity, json: {
            msg: I18n.t('msg.public.create_error'),
            errors: @user.errors
          }
        end
      end

      def forget
        @user = User.where(email: @json['email']).first
        if @user.nil?
          render status: :not_found
        else
          @user.send_forget_password_email
          render status: :ok
        end
      end

      def change_password_page
        renderer = create_change_password_page
        bm = Binding::SignupEmailBinding.new(@user)
        data = renderer.result(bm.generate_binding)
        render status: :ok, html: data.html_safe
      end

      def change_password
        @password = params[:password]
        @verify_password = params[:verify_password]
        if @password.length < 6 || (@password != @verify_password)
          render status: :unprocessable_entity, json: {
            msg: I18n.t('msg.public.password_invalid')
          }
        else
          @user.update(
            api_key: @user.generate_api_key,
            password: Digest::SHA1.hexdigest(@password)
          )
          render status: :ok
        end
      end

      private

      def user_by_api_key
        @user = User.where(api_key: params[:api_key]).first
        render status: :not_found if @user.nil?
      end
    end
  end
end
