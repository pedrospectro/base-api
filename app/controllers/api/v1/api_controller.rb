module Api
  module V1
    class ApiController < ApplicationController
      include ActionController::HttpAuthentication::Token::ControllerMethods
      before_action :authenticate

      protected

      def authenticate
        authenticate_token || render_unauthorized
      end

      def authenticate_token
        authenticate_with_http_token do |token, _options|
          @current_user = User.find_by(api_key: token, deleted_at: nil)
          set_locale if @current_user
        end
      end

      def render_unauthorized(realm = "Application")
        headers["WWW-Authenticate"] = %(Token realm="#{realm.delete('"')}")
        render status: :bad_request, json: { msg: "Unauthorized" }
      end

      def verify_pedido
        @pedido = Pedido.where(deleted_at: nil, id: params[:pedido_id]).first
        if @pedido.nil?
          render status: :not_found
        elsif @pedido.user.id.to_s != @current_user.id.to_s
          render status: :bad_request, json: {
            msg: I18n.t('msg.pedido.user_not_owner')
          }
        end
      end
    end
  end
end
