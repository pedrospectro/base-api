module Api
  module V1
    module Users
      class AddressesController < Api::V1::ApiController
        before_action :verify_postal_code, only: [:create]

        def index
          @location = Location.find(@current_user.location_id)
          @address = @location.address
          render json: {
            address: @address,
            location: @location
          }
        end

        def create
          create_address_to_user(@current_user)
        end
      end
    end
  end
end
