module Api
  module V1
    module Users
      class UsersController < Api::V1::ApiController
        before_action :verify_user
        @dif_user_msg = { msg: I18n.t('msg.user.another_user') }

        def show
          render status: :ok, json: @current_user
        end

        def update
          @json['deleted_at'] = @current_user.deleted_at
          if @current_user.update(@json)
            unless @json["password"].nil?
              @current_user.update(password: Digest::SHA1.hexdigest(@json['password']))
            end
            render status: :already_reported, json: @current_user
          else
            render status: :unprocessable_entity, json: {
              msg: I18n.t('msg.user.update_user_error'),
              errors: @current_user.errors
            }
          end
        end

        def destroy
          if @current_user.update(deleted_at: Time.now)
            render status: :already_reported, json: @current_user
          else
            render status: :unprocessable_entity, json: {
              msg: I18n.t('msg.user.delete_user_error'),
              errors: @current_user.errors
            }
          end
        end

        private

        def verify_user
          dif_user = params[:id].to_i != @current_user.id
          render status: :bad_request, json: @dif_user_msg if dif_user
        end
      end
    end
  end
end
