module Api
  module V1
    module Users
      class PaymentsController < Api::V1::ApiController
        before_action :verify_pedido
        before_action :verify_pedido_already_reported, only: [:create]

        def index
          render json: @pedido.payment
        end

        def create
          @payment = @pedido.create_payment(@json)
          if @payment.save
            response = @payment.checkout
            if response.errors.any?
              render status: :unprocessable_entity, json: {
                error_msg: response.errors.join("\n")
              }
            else
              render status: :created, json: {
                payment: @payment,
                pedido: @pedido,
                checkout_url: response.url
              }
            end
          else
            render status: :unprocessable_entity, json: { msg: @payment.errors.first }
          end
        end

        private

        def verify_pedido_already_reported
          processing = @pedido.status != 'ready'
          msg = I18n.t('msg.payment.processing')
          render status: :unprocessable_entity, json: { msg: msg } if processing
        end
      end
    end
  end
end
