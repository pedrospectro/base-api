module Api
  module V1
    module Users
      class PedidosController < Api::V1::ApiController
        before_action :pedido, only: [:show, :update, :destroy]
        before_action :item_list, only: [:create]

        def index
          render json: @current_user.pedidos
        end

        def show
          render status: :ok, json: @pedido
        end

        def destroy
          @pedido.update(deleted_at: Time.now)
          if @pedido.save
            render status: :already_reported, json: @pedido
          else
            render status: :unprocessable_entity
          end
        end

        def create
          @pedido = @current_user.pedidos.create(items: @item_list)
          if @pedido.save
            render status: :created, json: @pedido
          else
            render status: :unprocessable_entity, json: {
              msg: I18n.t('msg.pedido.create_error'),
              errors: @pedido.errors
            }
          end
        end

        private

        def pedido
          @pedido = Pedido.where(deleted_at: nil, id: params[:id]).first
          if @pedido.nil?
            render status: :not_found
          elsif @pedido.user_id.to_s != @current_user.id.to_s
            render status: :bad_request, json: {
              msg: I18n.t('msg.pedido.user_not_owner')
            }
          end
        end

        def item_list
          if @json['items'].nil? || @json['items'].empty?
            render status: :unprocessable_entity, json: {
              msg: I18n.t('validate.pedido.must_have_one_item_or_more')
            }
          else
            @item_list = []
            for @item in @json['items']
              @item_list << Item.create(@item)
            end
          end
        end
      end
    end
  end
end
