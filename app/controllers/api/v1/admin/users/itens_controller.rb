module Api
  module V1
    module Admin
      module Users
        class ItensController < Api::V1::Admin::AdminController
          before_action :user_info
          before_action :verify_user_pedido
          before_action :item, only: [:show]

          def index
            render json: @pedido.items
          end

          def show
            render status: :ok, json: @item
          end

          private

          def item
            @item = Item.where(deleted_at: nil, id: params[:id]).first
            if @item.nil?
              render status: :not_found
            elsif @item.pedido_id.to_s != @pedido.id.to_s
              render status: :bad_request, json: {
                msg: I18n.t('msg.item.not_belongs_to_pedido')
              }
            end
          end
        end
      end
    end
  end
end
