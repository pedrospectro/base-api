module Api
  module V1
    module Admin
      module Users
        class PedidosController < Api::V1::Admin::AdminController
          before_action :user_info
          before_action :pedido, only: [:show, :update, :destroy]

          def index
            render json: @user_info.pedidos
          end

          def show
            render status: :ok, json: @pedido
          end

          def destroy
            @pedido.update(deleted_at: Time.now)
            if @pedido.save
              render status: :already_reported, json: @pedido
            else
              render status: :unprocessable_entity
            end
          end

          def update
            @pedido.update(@json)
            if @pedido.save
              render status: :already_reported, json: @pedido
            else
              render status: :unprocessable_entity
            end
          end

          private

          def pedido
            @pedido = Pedido.where(id: params[:id]).first
            if @pedido.nil?
              render status: :not_found
            elsif @pedido.user_id.to_s != @user_info.id.to_s
              render status: :not_found, json: {
                msg: I18n.t('msg.pedido.user_not_owner')
              }
            end
          end
        end
      end
    end
  end
end
