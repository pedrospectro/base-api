module Api
  module V1
    module Admin
      module Users
        class PaymentsController < Api::V1::Admin::AdminController
          before_action :user_info
          before_action :verify_user_pedido
          def index
            render json: @pedido.payment
          end
        end
      end
    end
  end
end
