module Api
  module V1
    module Admin
      module Users
        class RoleUsersController < Api::V1::Admin::AdminController
          before_action :user_info
          before_action :posted_role, only: [:create]
          before_action :association, only: [:destroy]
          @msg_relation = I18n.t('msg.roles_user.not_found')

          def index
            @result = []
            @roles_users = RolesUser.where(user: @user_info, deleted_at: nil)
            for @roles in @roles_users
              @result.push(
                id: @roles.id,
                user_id: @roles.user_id,
                role_id: @roles.role_id,
                created_at: @roles.created_at,
                updated_at: @roles.updated_at,
                role: Role.where(id: @roles.role_id).first
              )
            end
            render json: @result
          end

          def create
            @association = RolesUser.create(role: @role, user: @user_info)
            if @association
              render status: :created, json: @association
            else
              render status: :unprocessable_entity
            end
          end

          def destroy
            @roles_users.destroy
            if @roles_users
              render status: :already_reported, json: @roles_users
            else
              render status: :unprocessable_entity
            end
          end

          private

          def posted_role
            @role = Role.where(id: @json["role_id"], deleted_at: nil).first
            render status: :not_found if @role.nil?
          end

          def association
            @roles_users = RolesUser.where(id: params[:id], deleted_at: nil).first
            render status: :not_found, json: { msg: @msg_relation } if @roles_users.nil?
          end
        end
      end
    end
  end
end
