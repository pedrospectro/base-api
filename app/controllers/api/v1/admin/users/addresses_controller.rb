module Api
  module V1
    module Admin
      module Users
        class AddressesController < Api::V1::Admin::AdminController
          before_action :user_info
          before_action :verify_postal_code, only: [:create]

          def index
            @location = Location.find(@user_info.location_id)
            @address = @location.address
            render json: {
              address: @address,
              location: @location
            }
          end

          def create
            create_address_to_user(@user_info)
          end
        end
      end
    end
  end
end
