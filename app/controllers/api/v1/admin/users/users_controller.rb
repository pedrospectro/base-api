module Api
  module V1
    module Admin
      module Users
        class UsersController < Api::V1::Admin::AdminController
          before_action :user, only: [:show, :destroy, :update]

          def index
            render json: admin_page_index(User, params)
          end

          def show
            render status: :ok, json: @user_info
          end

          def create
            @user = User.create(@json)
            if @user.valid?
              @user.update(password: Digest::SHA1.hexdigest(@json['password']))
              RolesUser.create(user: @user, role: Role.first)
              render status: :created, json: @user
            else
              render status: :unprocessable_entity, json: {
                msg: I18n.t('msg.admin_user.create_error'),
                errors: @user.errors
              }
            end
          end

          def update
            @json['deleted_at'] = @user_info.deleted_at
            if @user_info.update(@json)
              unless @json["password"].nil?
                @user_info.update(password: Digest::SHA1.hexdigest(@json['password']))
              end
              render status: :already_reported, json: @user_info
            else
              render status: :unprocessable_entity, json: {
                msg: I18n.t('msg.admin_user.update_error'),
                errors: @user_info.errors
              }
            end
          end

          def destroy
            if @user_info.update(deleted_at: Time.now)
              render status: :already_reported, json: @user_info
            else
              render status: :unprocessable_entity, json: {
                msg: I18n.t('msg.admin_user.delete_error'),
                errors: @user_info.errors
              }
            end
          end

          private

          def user
            @user_info = User.where(id: params[:id]).first
            render status: :not_found if @user_info.nil?
          end
        end
      end
    end
  end
end
