module Api
  module V1
    module Admin
      class AdminController < Api::V1::ApiController
        before_action :verify_admin

        private

        def verify_admin
          render status: :bad_request if @current_user.roles.where(name: 'admin').empty?
        end

        def user_info
          @user_info = User.where(id: params[:user_id]).first
          render status: :not_found if @user_info.nil?
        end

        def user_seller_info
          @user_info = User.where(id: params[:seller_id]).first
          if @user_info.nil?
            render status: :not_found
          elsif @user_info.roles.where(name: 'seller').empty?
            render status: :not_found
          end
        end

        def verify_user_pedido
          @pedido = Pedido.where(deleted_at: nil, id: params[:pedido_id]).first
          if @pedido.nil?
            render status: :not_found
          elsif @pedido.user.id.to_s != @user_info.id.to_s
            render status: :bad_request, json: {
              msg: I18n.t('msg.pedido.user_not_owner')
            }
          end
        end
      end
    end
  end
end
