module Api
  module V1
    module Admin
      class RolesController < Api::V1::Admin::AdminController
        before_action :role_info, only: [:show, :destroy, :update]

        def index
          render json: Role.where(deleted_at: nil)
        end

        def show
          if @role
            render status: :ok, json: @role
          else
            render status: :not_found
          end
        end

        def create
          @role = Role.create(@json)
          if @role.save
            render status: :created, json: @role
          else
            render status: :unprocessable_entity
          end
        end

        def destroy
          if @role.update(deleted_at: Time.now)
            render status: :already_reported, json: @role
          else
            render status: :unprocessable_entity
          end
        end

        def update
          if @role.update(@json)
            render status: :already_reported, json: @role
          else
            render status: :unprocessable_entity
          end
        end

        private

        def role_info
          @role = Role.where(id: params[:id]).first
          render status: :not_found if @role.nil?
        end
      end
    end
  end
end
