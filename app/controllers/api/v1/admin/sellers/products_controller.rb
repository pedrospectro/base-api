module Api
  module V1
    module Admin
      module Sellers
        class ProductsController < Api::V1::Admin::AdminController
          before_action :user_seller_info
          before_action :edit_product, only: [:show, :update, :destroy]
          @msg_error = I18n.t('msg.product.user_not_own')

          def index
            render json: @user_info.products
          end

          def show
            render json: @product
          end

          def create
            @product = @user_info.products.create(@json)
            if @product.save
              render status: :created, json: @product
            else
              @msg = @product.errors.messages.first
              render status: :unprocessable_entity, json: { msg: @msg }
            end
          end

          def update
            @product.update(@json)
            if @product.save
              render status: :already_reported, json: @product
            else
              @msg = @product.errors.messages.first
              render status: :unprocessable_entity, json: { msg: @msg }
            end
          end

          def destroy
            @product.update(deleted_at: Time.now)
            if @product.save
              render status: :already_reported
            else
              @msg = @product.errors.messages.first
              render status: :unprocessable_entity, json: { msg: @msg }
            end
          end

          private

          def edit_product
            @product = Product.where(id: params[:id]).first
            if @product.nil?
              render status: :not_found
            elsif @product.user_id != @user_info.id
              render status: :bad_request, json: { msg: @msg_error }
            end
          end
        end
      end
    end
  end
end
