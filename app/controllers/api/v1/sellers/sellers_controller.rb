module Api
  module V1
    module Sellers
      class SellersController < Api::V1::ApiController
        before_action :verify_seller

        def show
          render status: :ok, json: @current_user
        end

        private

        def verify_seller
          render status: :bad_request if @current_user.roles.where(name: 'seller').empty?
        end

        def user_info
          @user_info = User.where(id: params[:seller_id]).first
          render status: :not_found if @user_info.nil?
          if @user_info != @current_user
            render status: :bad_request, json: {
              msg: I18n.t('msg.product.user_not_own')
            }
          end
        end

        def fetch_product
          @product = Product.where(id: params[:id], deleted_at: nil).first
          if @product.nil?
            render status: :not_found
          elsif @product.user_id != @current_user.id
            render status: :bad_request, json: { msg: I18n.t('msg.product.user_not_own') }
          end
        end
      end
    end
  end
end
