module Api
  module V1
    module Sellers
      class ProductsController < Api::V1::Sellers::SellersController
        before_action :fetch_product, only: [:show, :update, :destroy]

        def index
          render json: @current_user.products
        end

        def show
          render json: @product
        end

        def create
          @product = @current_user.products.create(@json)
          if @product.save
            render status: :created, json: @product
          else
            @msg = @product.errors.messages.first
            render status: :unprocessable_entity, json: { msg: @msg }
          end
        end

        def update
          @product.update(@json)
          if @product.save
            render status: :already_reported, json: @product
          else
            @msg = @product.errors.messages.first
            render status: :unprocessable_entity, json: { msg: @msg }
          end
        end

        def destroy
          @product.update(deleted_at: Time.now)
          if @product.save
            render status: :already_reported
          else
            @msg = @product.errors.messages.first
            render status: :unprocessable_entity, json: { msg: @msg }
          end
        end
      end
    end
  end
end
