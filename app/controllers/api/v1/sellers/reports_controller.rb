module Api
  module V1
    module Sellers
      class ReportsController < Api::V1::Sellers::SellersController
        before_action :fetch_product_id, only: [:index]

        def index
          render json: report(@product)
        end

        def report(product)
          product
        end

        private

        def fetch_product_id
          @product = Product.where(id: params[:product_id], deleted_at: nil).first
          if @product.nil?
            render status: :not_found
          elsif @product.user_id != @current_user.id
            render status: :bad_request, json: { msg: I18n.t('msg.product.user_not_own') }
          end
        end
      end
    end
  end
end
