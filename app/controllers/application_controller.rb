class ApplicationController < ActionController::API
  serialization_scope :view_context

  before_action :parse_json, only: [
    :signup,
    :login,
    :forget,
    :create,
    :update
  ]

  def info
    render status: :ok, json: {
      msgs: 'api ok with tests ok',
      version: '1.0 beta',
      public_scope: ENV['API_VERSION'],
      user_scope: ENV['API_VERSION'] + '/users/',
      resources_route: 'rails/info/routes',
      doc_url: 'https://documenter.getpostman.com/view/710295/base-api/7Loe2PE'
    }
  end

  def role_info_by_name(name)
    Role.where(name: name).first
  end

  def page_index(model_name, params)
    if params[:page].nil? || params[:page_size].nil?
      @data = model_name.where(deleted_at: nil)
    else
      @data_count = model_name.where(deleted_at: nil).count
      @current_page = params[:page].to_i
      @page_size = params[:page_size].to_i
      @total_pages = (@data_count.to_f / @page_size.to_f).ceil
      @page_info = {
        current_page: @current_page,
        page_size: @page_size,
        data_count: @data_count,
        total_pages: @total_pages
      }
      @offset = @page_size * (@current_page - 1)
      @data = model_name.all.limit(@page_size).offset(@offset)
    end
    return @data if params[:page].nil? || params[:page_size].nil?
    { page_info: @page_info, page_data: @data }
  end

  def admin_page_index(model_name, params)
    if params[:page].nil? || params[:page_size].nil?
      @data = model_name.all
    else
      @data_count = model_name.count
      @current_page = params[:page].to_i
      @page_size = params[:page_size].to_i
      @total_pages = (@data_count.to_f / @page_size.to_f).ceil
      @page_info = {
        current_page: @current_page,
        page_size: @page_size,
        data_count: @data_count,
        total_pages: @total_pages
      }
      @offset = @page_size * (@current_page - 1)
      @data = model_name.all.limit(@page_size).offset(@offset)
    end
    return @data if params[:page].nil? || params[:page_size].nil?
    { page_info: @page_info, page_data: @data }
  end

  def get_address_by_postal_code(postal_code)
    @address = GetAddressByCepJob.perform_now(postal_code)
    unless @address[:street].nil?
      return {
        postal_code: postal_code,
        country: @address[:country],
        uf: @address[:uf],
        city: @address[:city],
        neigborhood: @address[:neighborhood],
        street: @address[:street]
      }
    end
    return nil if @address[:street].nil?
  end

  def create_address_to_user(user)
    cep_found = true
    @address = Address.find_by(postal_code: @json['postal_code'])
    if @address.nil?
      @data = get_address_by_postal_code(@json['postal_code'])
      if !@data.nil?
        @address = Address.create(@data)
        @coords = GetLocationByAddressJob.perform_now(
          @address,
          @json["street_number"]
        )
        @location = @address.locations.create(
          lat: @coords[:lat],
          lng: @coords[:lng],
          street_number: @coords[:number]
        )
      else
        cep_found = false
      end
    else
      @location = Location.find_by(
        address_id: @address.id,
        street_number: @json["street_number"]
      )
      if @location.nil?
        @coords = GetLocationByAddressJob.perform_now(
          @address,
          @json["street_number"]
        )
        @location = @address.locations.create(
          lat: @coords[:lat],
          lng: @coords[:lng],
          street_number: @coords[:number]
        )
      end
    end
    if cep_found
      user.update(location_id: @location.id)
      render status: :created, json: {
        address: @address,
        location: @location
      }
    else
      render status: :not_found
    end
  end

  def set_locale
    I18n.locale = @current_user.try(:locale_string) || I18n.default_locale
  end

  def locale_on_login(user)
    I18n.locale = user.try(:locale_string) || I18n.default_locale
  end

  private

  def parse_json
    @json = JSON.parse(request.body.read)
  end

  def verify_postal_code
    if @json['postal_code'].nil? || @json['postal_code'] == ''
      render status: :unprocessable_entity
    else
      @json['postal_code'] = @json['postal_code'].delete('-')
    end
  end
end
