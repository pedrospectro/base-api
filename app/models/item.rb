class Item < ApplicationRecord
  self.table_name = "itens"
  belongs_to :pedido

  validates :quantity, presence: { message: I18n.t('msg.item.quantity_presence') }
  validates :unity_value, presence: { message: I18n.t('msg.item.unity_value_presence') }

  before_create do
    total_value = (quantity * unity_value)
    self.total_value = total_value
    # pedido.update(total_value: total_value)
  end
end
