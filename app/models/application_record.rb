class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  @@name_regex = /\A\D+\s*(\D+\s*)*\z/
  @@product_name_regex = /\A\D+\z/
  @@email_regex = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  @@password_regex = /\A(\D|\w){6}(\D|\w)*\z/
end
