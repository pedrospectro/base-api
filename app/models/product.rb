class Product < ApplicationRecord
  validates :name, presence: true
  validates :category, presence: true
  validates :warranty, presence: true
  validates :value, presence: true
  validates :quantity, presence: true
  belongs_to :user
end
