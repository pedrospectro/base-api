class Pedido < ApplicationRecord
  belongs_to :user
  has_many :items
  validates :items, length: {
    minimum: 1,
    message: I18n.t('validate.pedido.must_have_one_item_or_more')
  }
  has_one :payment
  validates :payment, length: {
    maximum: 1,
    message: I18n.t('validate.pedido.must_have_one_payment')
  }

  before_create do
    self.status = 'ready'
  end

  after_create do
    total_value = 0
    for item in items
      total_value += item.total_value
    end
    update(total_value: total_value)
    send_msg
  end

  def send_msg
    msg = pedido_created_msg
    Telegram::SendTelegramMsgJob.perform_now(msg)
    Slack::SendSlackMsgJob.perform_now(msg, I18n.t('slack_emoji.money'))
    Email::PedidoEmailJob.perform_now(self)
  end

  def pedido_created_msg
    I18n.t(
      'telegram.pedido_created',
      user_name: user.name,
      pedido_id: id.to_s,
      pedido_total_value: total_value.to_s
    )
  end

  def process_notification(transaction, status)
    Pagseguro::ProcessNotificationJob.perform_now(transaction, status)
  end
end
