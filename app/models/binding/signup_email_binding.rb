class Binding
  class SignupEmailBinding
    attr_accessor :user

    def initialize(user)
      @user = user
    end

    def generate_binding
      binding
    end
  end
end
