class Binding
  class PedidoEmailBinding
    attr_accessor :user, :pedido

    def initialize(user, pedido)
      @user = user
      @pedido = pedido
    end

    def generate_binding
      binding
    end
  end
end
