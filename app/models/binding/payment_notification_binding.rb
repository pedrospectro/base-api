class Binding
  class PaymentNotificationBinding
    attr_accessor :user, :pedido, :item_list

    def initialize(user, pedido, item_list)
      @user = user
      @pedido = pedido
      @item_list = item_list
    end

    def generate_binding
      binding
    end
  end
end
