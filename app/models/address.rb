class Address < ApplicationRecord
  validates :postal_code, presence: true
  has_many :locations
end
