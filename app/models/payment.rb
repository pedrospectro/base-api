class Payment < ApplicationRecord
  belongs_to :pedido
  validates :provider, presence: { message: I18n.t('msg.payment.provider_presence') }

  before_create do
    self.status = 'ready'
  end

  after_create do
    send_checkout_message
  end

  def checkout
    Pagseguro::ProcessCheckoutJob.perform_now(pedido)
  end

  def send_checkout_message
    pedido = self.pedido
    msg = checkout_created_msg(pedido.user, pedido)
    Telegram::SendTelegramMsgJob.perform_now(msg)
    Slack::SendSlackMsgJob.perform_now(msg, I18n.t('slack_emoji.money'))
  end

  def checkout_created_msg(user, pedido)
    I18n.t(
      'telegram.checkout_created',
      user_name: user.name,
      pedido_id: pedido.id.to_s,
      pedido_total_value: pedido.total_value.to_s
    )
  end
end
