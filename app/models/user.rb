class User < ApplicationRecord
  validates :name, format: {
    with: @@name_regex,
    message: I18n.t('validate.user.name')
  }

  validates :email, format: {
    with: @@email_regex,
    message: I18n.t('validate.user.email')
  }, uniqueness: { message: I18n.t('validate.user.email_taken') }

  validates :password, format: {
    with: @@password_regex,
    message: I18n.t('validate.user.password')
  }

  # Relationships
  has_and_belongs_to_many :roles
  has_many :pedidos
  has_many :products

  # Assign an API key on create
  before_create :assign_api_key
  after_create :send_signup_email

  def assign_api_key
    self.api_key = generate_api_key
  end

  # Generate a unique API key
  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token unless User.exists?(api_key: token)
    end
  end

  def send_signup_email
    Email::SignupEmailJob.perform_now(self)
  end

  def send_forget_password_email
    Email::ForgetPasswordEmailJob.perform_now(self)
  end
end
