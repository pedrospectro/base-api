module PublicControllerHelper
  def create_change_password_page
    erb_file = "app/views/layouts/change_password_page.html.erb"
    erb_str = File.read(erb_file)
    renderer = ERB.new(erb_str)
    renderer
  end
end
