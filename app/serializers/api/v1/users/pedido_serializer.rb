module Api
  module V1
    module Users
      class PedidoSerializer < ActiveModel::Serializer
        attributes :id, :status, :total_value
        has_many :items
      end
    end
  end
end
