module Api
  module V1
    class UserSerializer < ActiveModel::Serializer
      attributes  :id, :name, :email,
                  :dob, :api_key, :cpf,
                  :created_at, :updated_at,
                  :deleted_at, :locale_string
      has_many :roles
    end
  end
end
