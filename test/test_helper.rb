require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/autorun'

module ActiveSupport
  class TestCase
    include FactoryGirl::Syntax::Methods
    # setup { DatabaseCleaner.start }
    # teardown { DatabaseCleaner.clean }
    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    setup do
      @user_keys = %w[
        id name email dob api_key cpf
        created_at updated_at deleted_at roles locale_string
      ]

      @default_item = {
        quantity: 1,
        unity_value: 1
      }
      @next_default_item = {
        quantity: 1,
        unity_value: 1
      }
      @user = FactoryGirl.create(:payment_user)
      @pedido = FactoryGirl.create(
        :payment_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @without_payment_pedido = FactoryGirl.create(
        :without_payment_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @item = FactoryGirl.create(:payment_item)
      @without_payment_item = FactoryGirl.create(:without_payment_item)
      @payment = FactoryGirl.create(:pagseguro_payment)
      @another_user = FactoryGirl.create(:another_payment_user)
      @another_pedido = FactoryGirl.create(
        :another_payment_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @without_another_payment_pedido = FactoryGirl.create(
        :without_another_payment_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @another_item = FactoryGirl.create(:another_payment_item)
      @without_another_payment_item = FactoryGirl.create(:without_another_payment_item)
      @another_payment = FactoryGirl.create(:another_pagseguro_payment)
      @with_another_payment_mock = FactoryGirl.create(
        :with_another_payment_mock,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )

      @itens_another_user = FactoryGirl.create(:another_item_user)
      @itens_another_pedido = FactoryGirl.create(
        :another_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @itens_another_item = FactoryGirl.create(:another_item)
      @itens_user = FactoryGirl.create(:item_user)
      @itens_pedido = FactoryGirl.create(
        :ready_pedido,
        items: [
          Item.create(@default_item),
          Item.create(@next_default_item)
        ]
      )
      @itens_item = FactoryGirl.create(:created_item)
    end

    def version_test
      ENV['API_VERSION']
    end

    def parse_json(data)
      ::JSON.parse(data.to_json)
    end

    module Minitest
      class Spec
        include FactoryGirl::Syntax::Methods
      end
    end
    # Add more helper methods to be used by all tests here...
  end
end
