require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  test "create product" do
    user_data = users(:user_one_seller)
    post version_test + '/sellers/' + user_data.id.to_s + '/products/', as: :json, params: {
      name: "test name",
      category: "category test",
      warranty: "warranty test",
      quantity: 100,
      value: 100.9
    }, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :created
  end

  test "create product without athenticated" do
    user_data = users(:user_one)
    post version_test + '/sellers/' + user_data.id.to_s + '/products/', as: :json, params: {
      name: "test name",
      category: "category test",
      warranty: "warranty test",
      quantity: 100,
      value: 100.9
    }
    assert_response :bad_request
  end

  test "create product with another seller" do
    user_data = users(:user_two_seller)
    user_another = users(:user_one_seller)
    post version_test + '/sellers/' + user_another.id.to_s + '/products/', as: :json, params: {
      name: "test name",
      category: "category test",
      warranty: "warranty test",
      quantity: 100,
      value: 100.9
    }, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :bad_request
  end

  test "create product with empty name" do
    user_data = users(:user_one_seller)
    post version_test + '/sellers/' + user_data.id.to_s + '/products/', as: :json, params: {
      name: "",
      category: "category test",
      warranty: "warranty test",
      quantity: 100,
      value: 100.9
    }, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :unprocessable_entity
  end

  test "update product not found" do
    user_data = users(:user_one_seller)
    put(
      api_v1_seller_product_url(user_data, products(:deleted_product)),
      as: :json,
      params: { name: "new name" },
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :not_found
  end

  test "update product with empty_name" do
    user_data = users(:user_one_seller)
    put(
      api_v1_seller_product_url(user_data, products(:product)),
      as: :json,
      params: { name: "" },
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :unprocessable_entity
  end

  test "update product" do
    user_data = users(:user_one_seller)
    put(
      api_v1_seller_product_url(user_data, products(:product)),
      as: :json,
      params: { name: "new name" },
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "update product from another user" do
    user_data = users(:user_one_seller)
    put(
      api_v1_seller_product_url(user_data, products(:product_another_user)),
      as: :json,
      params: { name: "new name" },
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "delete product not found" do
    user_data = users(:user_one_seller)
    delete(
      api_v1_seller_product_url(user_data, products(:deleted_product)),
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :not_found
  end

  test "delete product" do
    user_data = users(:user_one_seller)
    delete(
      api_v1_seller_product_url(user_data, products(:product)),
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "get product" do
    user_data = users(:user_one_seller)
    get(
      api_v1_seller_product_url(user_data, products(:product)),
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "index products" do
    user_data = users(:user_one_seller)
    get(
      api_v1_seller_products_url(user_data),
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end
end
