require 'test_helper'

class ReportsControllerTest < ActionDispatch::IntegrationTest
  test "index report product" do
    user_data = users(:user_one_seller)
    product = products(:product)
    get(
      api_v1_seller_product_reports_url(user_data, product),
      headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end
end
