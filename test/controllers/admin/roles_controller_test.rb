require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest
  test "get all roles withouth admin token" do
    user_data = users(:not_admin)
    get(
      api_v1_admin_roles_url, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "get all roles" do
    user_data = users(:admin)
    get(
      api_v1_admin_roles_url, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "get roles" do
    user_data = users(:admin)
    role_data = roles(:user)
    get(
      api_v1_admin_role_url(role_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "get roles not_found" do
    user_data = users(:admin)
    get version_test + '/admin/roles/0', headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :not_found
  end

  test "get roles deleted" do
    user_data = users(:admin)
    role_data = roles(:deleted_role)
    get(
      api_v1_admin_role_url(role_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "update roles" do
    user_data = users(:admin)
    role_data = roles(:user)
    put(
      api_v1_admin_role_url(role_data), as: :json, params: {
        name: 'new_name'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "delete roles" do
    user_data = users(:admin)
    role_data = roles(:user)
    delete(
      api_v1_admin_role_url(role_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "get deleted roles" do
    user_data = users(:admin)
    role_data = roles(:deleted_role)
    get(
      api_v1_admin_role_url(role_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end
end
