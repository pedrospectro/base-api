require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  test "create product with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    post(
      api_v1_admin_seller_products_url(user_data), as: :json, params: {
        name: "test name",
        category: "category test",
        warranty: "warranty test",
        quantity: 100,
        value: 100.9
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :created
  end

  test "create product with empty name and admin token" do
    user_admin = users(:admin)
    user_data = users(:user_one_seller)
    post(
      api_v1_admin_seller_products_url(user_data), as: :json, params: {
        name: "",
        category: "category test",
        warranty: "warranty test",
        quantity: 100,
        value: 100.9
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :unprocessable_entity
  end

  test "create product without admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:not_admin)
    post(
      api_v1_admin_seller_products_url(user_data), as: :json, params: {
        name: "test name",
        category: "category test",
        warranty: "warranty test",
        quantity: 100,
        value: 100.9
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :bad_request
  end

  test "update product deleted with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    put(
      api_v1_admin_seller_product_url(
        user_data,
        products(:deleted_product)
      ), as: :json, params: { name: "new name" }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "update product with empty_name with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    put(
      api_v1_admin_seller_product_url(
        user_data,
        products(:product)
      ), as: :json, params: { name: nil }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :unprocessable_entity
  end

  test "update product without admin token" do
    user_data = users(:user_one_seller)
    put(
      api_v1_admin_seller_product_url(
        user_data,
        products(:product)
      ), as: :json, params: { name: "new name" }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "update product with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    put(
      api_v1_admin_seller_product_url(
        user_data,
        products(:product)
      ), as: :json, params: { name: "new name" }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "delete product not found with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    delete(
      api_v1_admin_seller_product_url(
        user_data,
        products(:deleted_product)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "delete product with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    delete(
      api_v1_admin_seller_product_url(
        user_data,
        products(:product)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "index products with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    get(
      api_v1_admin_seller_products_url(
        user_data
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :ok
  end

  test "get products id with admin token" do
    user_data = users(:user_one_seller)
    user_admin = users(:admin)
    get(
      api_v1_admin_seller_product_url(
        user_data,
        products(:product)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :ok
  end
end
