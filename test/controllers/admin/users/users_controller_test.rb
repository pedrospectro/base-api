require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "get user without admin token" do
    user_data = users(:not_admin)
    get(
      version_test + '/admin/users/1', headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "get user with admin token" do
    user_data = users(:admin)
    get(
      version_test + '/admin/users/1', headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "index users without admin token" do
    user_data = users(:not_admin)
    get(
      api_v1_admin_users_url, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "index users with admin token" do
    user_data = users(:admin)
    get(
      api_v1_admin_users_url, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "update user without admin token" do
    put(
      version_test + '/admin/users/1', as: :json, params: {
        api_key: 'new_api_key'
      }
    )
    assert_response :bad_request
  end

  test "update user with admin token" do
    user_data = users(:admin)
    put(
      version_test + '/admin/users/1', as: :json, params: {
        api_key: 'new_api_key'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "delete user to be deleted without admin token" do
    user_data = users(:not_admin)
    user_to_delete = users(:user_to_delete_by_admin)
    delete(
      api_v1_admin_user_url(user_to_delete), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "delete user to be deleted with admin token" do
    user_data = users(:admin)
    user_to_delete = users(:user_to_delete_by_admin)
    delete(
      api_v1_admin_user_url(user_to_delete), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end

  test "get deleted user with admin token" do
    user_data = users(:admin)
    user_deleted = users(:deleted_user)
    get(
      api_v1_admin_user_url(user_deleted), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "get deleted user without admin token" do
    user_data = users(:not_admin)
    user_deleted = users(:deleted_user)
    get(
      api_v1_admin_user_url(user_deleted), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "test create user without admin token" do
    user_data = users(:not_admin)
    post(
      api_v1_admin_users_url, as: :json, params: {
        name: "Pedro Admin Teste",
        email: "test_new_user_by_admin@signup.com",
        password: "test_password"
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "test create user with admin token" do
    user_data = users(:admin)
    post(
      api_v1_admin_users_url, as: :json, params: {
        name: "Pedro Admin Teste",
        email: "test_new_user_by_admin@signup.com",
        password: "test_password"
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :created
  end

  test "test create user with admin token and invalid user" do
    user_data = users(:admin)
    post(
      api_v1_admin_users_url, as: :json, params: {
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :unprocessable_entity
  end
end
