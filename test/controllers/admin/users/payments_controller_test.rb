require 'test_helper'

class PaymentsControllerTest < ActionDispatch::IntegrationTest
  test "index should return bad request without admin token" do
    get(
      api_v1_admin_user_pedido_payments_url(@user, @pedido), headers: {
        "Authorization" => "Token token=" + @user.api_key
      }
    )
    assert_response :bad_request
  end

  test "admin index should return bad_request with message pedido user not owner" do
    user_admin = users(:admin)
    get(
      api_v1_admin_user_pedido_payments_url(@another_user, @pedido), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert response.body.include?(I18n.t('msg.pedido.user_not_owner'))
    assert_response :bad_request
  end

  test "admin index should return ok" do
    user_admin = users(:admin)
    get(
      api_v1_admin_user_pedido_payments_url(@user, @pedido), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :ok
  end
end
