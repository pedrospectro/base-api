require 'test_helper'

class PedidosControllerTest < ActionDispatch::IntegrationTest
  test "Index Orders To user_valid withouth admin token" do
    user_data = users(:not_admin)
    get version_test + '/admin/users/1/pedidos', headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :bad_request
  end

  test "Index Orders To user_valid with admin token" do
    user_data = users(:admin)
    get version_test + '/admin/users/1/pedidos', headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :ok
  end

  test "Index Orders To deleted_user with admin token" do
    user_data = users(:admin)
    deleted_user = users(:deleted_user)
    get(
      api_v1_admin_user_pedidos_url(deleted_user), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end

  test "get pedido from another user with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    get(
      api_v1_admin_user_pedido_url(user_data, pedidos(:pedido_deleted)), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :not_found
  end

  test "get pedido ok with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    get(
      api_v1_admin_user_pedido_url(user_data, pedidos(:pedido_ok)), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :ok
  end

  test "update Pedido with admin token" do
    user_admin = users(:admin)
    user_data = users(:user_one)
    pedidos(:pedido_ok).items.create(@default_item)
    put(
      api_v1_admin_user_pedido_url(
        user_data,
        pedidos(:pedido_ok)
      ), as: :json, params: {
        total_value: 100.99,
        status: "ready"
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "update Pedido without admin token" do
    user_admin = users(:not_admin)
    user_data = users(:user_one)
    put(
      api_v1_admin_user_pedido_url(user_data, pedidos(:pedido_ok)), as: :json, params: {
        total_value: 100.99,
        status: "ready"
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :bad_request
  end

  test "delete Pedido with admin token" do
    user_admin = users(:admin)
    user_data = users(:user_one)
    pedidos(:pedido_ok).items.create(@default_item)
    delete(
      api_v1_admin_user_pedido_url(
        user_data,
        pedidos(:pedido_ok)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "delete Pedido without admin token" do
    user_admin = users(:not_admin)
    user_data = users(:user_one)
    delete(
      api_v1_admin_user_pedido_url(user_data, pedidos(:pedido_ok)), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :bad_request
  end
end
