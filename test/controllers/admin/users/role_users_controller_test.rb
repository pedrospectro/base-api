require 'test_helper'

class RoleUsersControllerTest < ActionDispatch::IntegrationTest
  test "Index Roles To user_valid withouth admin token" do
    user_data = users(:user_one)
    user_admin = users(:not_admin)
    get(
      api_v1_admin_user_role_users_url(user_data), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :bad_request
  end

  test "Index Roles To user_valid with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    get(
      api_v1_admin_user_role_users_url(user_data), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :ok
  end

  test "Create RoleUsers Many to Many Association To user_valid with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    post(
      api_v1_admin_user_role_users_url(user_data), as: :json, params: {
        role_id: roles(:admin).id.to_s
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :created
  end

  test "Create RoleUsers (deleted_role) Many to Many Association with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    post(
      api_v1_admin_user_role_users_url(user_data), as: :json, params: {
        role_id: roles(:deleted_role).id.to_s
      }, headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :not_found
  end

  test "destroy roleUser Association without admin token" do
    user_data = users(:user_test)
    user_admin = users(:not_admin)
    delete(
      api_v1_admin_user_role_user_url(
        user_data, roles_users(:user_test_role_one)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :bad_request
  end

  test "destroy roleUser Association with admin token" do
    user_data = users(:user_test)
    user_admin = users(:admin)
    delete(
      api_v1_admin_user_role_user_url(
        user_data,
        roles_users(:user_test_role_one)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :already_reported
  end

  test "destroy roleUser deleted Association with admin token" do
    user_data = users(:user_one)
    user_admin = users(:admin)
    delete(
      api_v1_admin_user_role_user_url(
        user_data,
        roles_users(:user_one_role_four)
      ), headers: {
        "Authorization" => "Token token=" + user_admin.api_key
      }
    )
    assert_response :not_found
  end
end
