require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  test 'create notification' do
    post(
      api_v1_notifications_url,
      as: :json,
      params: {
        notificationCode: 'teste_code'
      }
    )
    assert_response :ok
  end
end
