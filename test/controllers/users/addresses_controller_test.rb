require 'test_helper'

class AddressesControllerTest < ActionDispatch::IntegrationTest
  test "Create Address To user_valid withouth token" do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: '80030010',
        street_number: '1'
      }
    )
    assert_response :bad_request
  end

  test "Create Address To user_valid without postal code" do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: nil,
        street_number: '1'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :unprocessable_entity
  end

  test "Create Address To user_valid with not_found postal code" do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: '00000000',
        street_number: '1'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :not_found
  end

  test "Create Address and Location To user_valid containing extra characters" do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: '80030--010',
        street_number: '1'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :created
  end

  test "Create and update stored Address with new  Location with valid postal code " do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: '80030010',
        street_number: '2'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :created
  end

  test "Create Address and Location To user_valid with valid end stored postal code" do
    user_data = users(:valid_user)
    post(
      api_v1_user_addresses_url(user_data), as: :json, params: {
        postal_code: '68743-010',
        street_number: '1'
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :created
  end

  test "Get address and location to valid_user without token" do
    user_data = users(:valid_user)
    get(api_v1_user_addresses_url(user_data))
    assert_response :bad_request
  end

  test "Get address and location to valid_user with token" do
    user_data = users(:valid_user)
    get(
      api_v1_user_addresses_url(user_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :ok
  end
end
