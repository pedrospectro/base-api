require 'test_helper'

class ItensControllerTest < ActionDispatch::IntegrationTest

  test "index should return bad request" do
    get(api_v1_user_pedido_itens_url(@itens_user, @itens_pedido))
    assert_response :bad_request
  end

  test "index should return bad_request with message pedido user not owner" do
    get(
      api_v1_user_pedido_itens_url(@itens_another_user, @itens_pedido), headers: {
        "Authorization" => "Token token=" + @itens_another_user.api_key
      }
    )
    assert response.body.include?(I18n.t('msg.pedido.user_not_owner'))
    assert_response :bad_request
  end

  test "index should return ok" do
    get(
      api_v1_user_pedido_itens_url(@itens_user, @itens_pedido), headers: {
        "Authorization" => "Token token=" + @itens_user.api_key
      }
    )
    assert_response :ok
  end

  test "get should return bad request" do
    get(
      api_v1_user_pedido_iten_url(
        @itens_another_user,
        @itens_another_pedido,
        @itens_another_item
      )
    )
    assert_response :bad_request
  end

  test "get should return bad_request with message pedido user not owner" do
    get(
      api_v1_user_pedido_iten_url(
        @itens_another_user,
        @itens_pedido,
        @itens_another_item
      ), headers: {
        "Authorization" => "Token token=" + @itens_another_user.api_key
      }
    )
    assert response.body.include?(I18n.t('msg.pedido.user_not_owner'))
    assert_response :bad_request
  end

  test "get should return bad_request with message item does not belong to pedido" do
    get(
      api_v1_user_pedido_iten_url(
        @itens_another_user,
        @itens_another_pedido,
        @itens_item
      ), headers: {
        "Authorization" => "Token token=" + @itens_another_user.api_key
      }
    )
    assert response.body.include?(I18n.t('msg.item.not_belongs_to_pedido'))
    assert_response :bad_request
  end

  test "get should return ok" do
    get(
      api_v1_user_pedido_iten_url(
        @itens_another_user,
        @itens_another_pedido,
        @itens_another_item
      ), headers: {
        "Authorization" => "Token token=" + @itens_another_user.api_key
      }
    )
    assert_response :ok
  end
end
