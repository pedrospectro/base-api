require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def new_api_key
    'new_api_key'
  end

  test "get user withouth token" do
    get version_test + '/users/1'
    assert_response :bad_request
  end

  test "get user with false token" do
    get version_test + '/users/1', headers: {
      "Authorization" => "Token token=token_not_exists"
    }
    assert_response :bad_request
  end

  test "get user with token" do
    user_data = users(:valid_user)
    get version_test + '/users/' + user_data.id.to_s, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }

    assert JSON.parse(response.body).keys.sort == @user_keys.sort
    assert_response :ok
  end

  test "get user with another user token" do
    user_data = users(:valid_user)
    another_user_data = users(:invalid_name)
    get version_test + '/users/' + another_user_data.id.to_s, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :bad_request
  end

  test "update user without token" do
    user_data = users(:valid_user)
    put version_test + '/users/' + user_data.id.to_s, as: :json, params: {
      api_key: new_api_key
    }
    assert_response :bad_request
  end

  test "update user with token" do
    user_data = users(:valid_user)
    put version_test + '/users/' + user_data.id.to_s, as: :json, params: {
      api_key: new_api_key
    }, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :already_reported
  end

  test "delete user to be deleted withouth token" do
    user_data = users(:user_to_delete)
    delete version_test + '/users/' + user_data.id.to_s, headers: {
      "Authorization" => "Token token="
    }
    assert_response :bad_request
  end

  test "delete user to be deleted" do
    user_data = users(:user_to_delete)
    delete version_test + '/users/' + user_data.id.to_s, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :already_reported
  end

  test "get deleted user with token" do
    user_data = users(:deleted_user)
    get version_test + '/users/' + user_data.id.to_s, headers: {
      "Authorization" => "Token token=" + user_data.api_key
    }
    assert_response :bad_request
  end
end
