require 'test_helper'

class PedidosControllerTest < ActionDispatch::IntegrationTest
  test "Index Orders To user_valid withouth token" do
    user_data = users(:user_one)
    get(api_v1_user_pedidos_url(user_data))
    assert_response :bad_request
  end

  test "Index Orders To user_valid with token" do
    user_data = users(:user_one)
    get(
      api_v1_user_pedidos_url(user_data), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    keys = %w[
      id status total_value items
    ]

    assert JSON.parse(response.body).first.keys.sort == keys.sort
    assert_response :ok
  end

  test "get pedido deleted" do
    user_data = users(:user_one)
    get(
      api_v1_user_pedido_url(user_data, pedidos(:pedido_deleted)), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :not_found
  end

  test "get pedido ok" do
    user_data = users(:user_one)
    get(
      api_v1_user_pedido_url(user_data, pedidos(:pedido_ok)), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )

    keys = %w[
      id status total_value items
    ]

    assert JSON.parse(response.body).keys.sort == keys.sort
    assert_response :ok
  end

  test "Create Pedido" do
    user_data = users(:user_one)
    post(
      api_v1_user_pedidos_url(user_data), as: :json, params: {
        items: [
          {
            quantity: 2,
            unity_value: 3
          },
          {
            quantity: 2,
            unity_value: 3
          }
        ]
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :created
  end

  test "get pedido from another user" do
    user_data = users(:user_one)
    get(
      api_v1_user_pedido_url(user_data, pedidos(:pedido_another_user)), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :bad_request
  end

  test "Create Pedido without items" do
    user_data = users(:user_one)
    post(
      api_v1_user_pedidos_url(user_data), as: :json, params: {
        items: nil
      }, headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :unprocessable_entity
  end

  test "delete Pedido With Item" do
    pedidos(:pedido_ok).items.create(@default_item)
    user_data = users(:user_one)
    delete(
      api_v1_user_pedido_url(
        user_data,
        pedidos(:pedido_ok)
      ), headers: {
        "Authorization" => "Token token=" + user_data.api_key
      }
    )
    assert_response :already_reported
  end
end
