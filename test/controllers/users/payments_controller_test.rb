require 'test_helper'

class PaymentsControllerTest < ActionDispatch::IntegrationTest
  test "index should return bad request" do
    get(api_v1_user_pedido_payments_url(@user, @pedido))
    assert_response :bad_request
  end

  test "index should return bad_request with message pedido user not owner" do
    get(
      api_v1_user_pedido_payments_url(@another_user, @pedido), headers: {
        "Authorization" => "Token token=" + @another_user.api_key
      }
    )
    assert response.body.include?(I18n.t('msg.pedido.user_not_owner'))
    assert_response :bad_request
  end

  test "index should return ok" do
    get(
      api_v1_user_pedido_payments_url(@user, @pedido), headers: {
        "Authorization" => "Token token=" + @user.api_key
      }
    )
    assert_response :ok
  end

  test "create should return bad request" do
    post(
      api_v1_user_pedido_payments_url(
        @user,
        @without_payment_pedido
      ), as: :json, headers: {
        "Authorization" => "Token token=" + @another_user.api_key
      }, params: {
        provider: 'pagseguro'
      }
    )
    assert_response :bad_request
  end

  test "create should return bad_request with message pedido user not owner" do
    post(
      api_v1_user_pedido_payments_url(
        @another_user,
        @without_payment_pedido
      ), as: :json, headers: {
        "Authorization" => "Token token=" + @another_user.api_key
      }, params: {
        provider: 'pagseguro'
      }
    )
    assert response.body.include?(I18n.t('msg.pedido.user_not_owner'))
    assert_response :bad_request
  end

  test "create should return ok" do
    post(
      api_v1_user_pedido_payments_url(
        @another_user,
        @without_another_payment_pedido
      ), as: :json, headers: {
        "Authorization" => "Token token=" + @another_user.api_key
      }, params: {
        provider: 'pagseguro'
      }
    )
    assert_response :created
  end

  test "create should return unprocessable entity with empty fields" do
    post(
      api_v1_user_pedido_payments_url(
        @another_user,
        @without_another_payment_pedido
      ), as: :json, headers: {
        "Authorization" => "Token token=" + @another_user.api_key
      }, params: {
        provider: nil
      }
    )
    assert_response :unprocessable_entity
  end

  test "create payment with pedido paid must return unproc entity message" do
    with_payment_pedido = create(:paid_pedido, items: [Item.create(@default_item)])
    with_payment_pedido.update(status: 'paid')
    another_user = with_payment_pedido.user
    create(:paid_payment)
    post(
      api_v1_user_pedido_payments_url(
        another_user,
        with_payment_pedido
      ), as: :json, headers: {
        "Authorization" => "Token token=" + another_user.api_key
      }, params: {
        provider: 'pagseguro'
      }
    )
    assert response.body.include?(I18n.t('msg.payment.processing'))
    assert_response :unprocessable_entity
  end
end
