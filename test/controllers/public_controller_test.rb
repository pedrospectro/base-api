require 'test_helper'

class PublicControllerTest < ActionDispatch::IntegrationTest
  @data_search = "one"

  test "post login public controller not found user" do
    post version_test + '/login/', as: :json, params: {
    }
    assert_response :not_found
  end

  test "post login public controller ok" do
    user_data = users(:valid_user)
    post version_test + '/login/', as: :json, params: {
      email: user_data.email,
      password: 'MyString'
    }

    assert JSON.parse(response.body).keys.sort == @user_keys.sort
    assert_response :ok
  end

  test "post signup public controller" do
    post version_test + '/signup/', as: :json, params: {
      name: "Pedro",
      email: "test@signup.com",
      password: "test_password"
    }

    assert JSON.parse(response.body).keys.sort == @user_keys.sort
    assert_response :created
  end

  test "post signup with empty user public controller" do
    post version_test + '/signup/', as: :json, params: {
    }
    assert_response :unprocessable_entity
  end

  test "test public search controller" do
    post version_test + '/search/', as: :json, params: {
      search: @data_search
    }
    assert_response :ok
  end

  test "post forget password public controller" do
    user = create(:forget_user)
    post version_test + '/forget/', as: :json, params: {
      email: user.email
    }
    assert_response :ok
  end

  test "post forget password public controller not found user" do
    post version_test + '/forget/', as: :json, params: {
      email: 'testestestes@forget.com'
    }
    assert_response :not_found
  end

  test "get change password public controller" do
    user = create(:forget_user)
    get version_test + '/change_password/' + user.api_key
    assert_response :ok
  end

  test "get change password public controller with invalid user" do
    get version_test + '/change_password/invaliTokenqwedsajf'
    assert_response :not_found
  end

  test "post change password public controller" do
    user = create(:forget_user)
    post version_test + '/change_password/' + user.api_key, as: :json, params: {
      password: 'abcdef',
      verify_password: 'abcdef'
    }
    assert_response :ok
  end

  test "post change password public controller with invalid user" do
    post version_test + '/change_password/invaliTokenqwedsajf', as: :json, params: {
      password: 'abcdef',
      verify_password: 'abcdef'
    }
    assert_response :not_found
  end

  test "post change password public controller with not verified password" do
    user = create(:forget_user)
    post version_test + '/change_password/' + user.api_key, as: :json, params: {
      password: 'abcdef',
      verify_password: 'fedcba'
    }
    assert_response :unprocessable_entity
  end

  test "post change password public controller with password length less than 6" do
    user = create(:forget_user)
    post version_test + '/change_password/' + user.api_key, as: :json, params: {
      password: 'abc',
      verify_password: 'abc'
    }
    assert_response :unprocessable_entity
  end

  test "post login public controller ok and test user pt-BR locale" do
    user_data = create(:ptbr_user)
    user_data.save
    post version_test + '/login/', as: :json, params: {
      email: user_data.email,
      password: 'password'
    }
    assert I18n.locale.to_s == 'pt-BR'
    assert_response :ok
  end

  test "post login public controller ok and test user another locale" do
    user_data = create(:en_user)
    user_data.save
    post version_test + '/login/', as: :json, params: {
      email: user_data.email,
      password: 'password'
    }
    assert I18n.locale.to_s == 'en'
    assert_response :ok
  end
end
