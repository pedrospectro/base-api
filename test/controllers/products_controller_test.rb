require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  test "get all products" do
    get version_test + '/products'
    assert_response :ok
  end

  test "get all products with page params" do
    get version_test + '/products?page=1&page_size=100'
    assert_response :ok
  end

  test "get one products with params id" do
    get version_test + '/products/' + products(:product).id.to_s
    assert_response :ok
  end

  test "get deleted products with params id" do
    get version_test + '/products/' + products(:deleted_product).id.to_s
    assert_response :not_found
  end
end
