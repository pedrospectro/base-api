require 'test_helper'

class UserTest < ActionDispatch::IntegrationTest
  test "Create User Without Data model" do
    user = User.new
    assert_not user.save
  end

  test "Create User With Invalid Name" do
    user_data = users(:invalid_name)
    user = User.create(parse_json(user_data))
    msg = I18n.t('validate.user.name')
    assert user.errors.messages[:name].first == msg
  end

  test "Create User With Invalid Email" do
    user_data = users(:invalid_email)
    user = User.create(parse_json(user_data))
    msg = I18n.t('validate.user.email')
    assert user.errors.messages[:email].first == msg
  end

  test "Create Valid User" do
    user = User.new(
      name: 'UserName',
      email: 'pedro@teste.api',
      password: 'password',
      dob: '2017-12-20 01:30:59',
      deleted_at: nil,
      api_key: 'api_key',
      cpf: '88923460249'
    )
    assert user.save
  end

  test "Create Valid User with email taken" do
    user_data = users(:valid_user)
    user = User.create(parse_json(user_data))
    msg = I18n.t('validate.user.email_taken')
    assert user.errors.messages[:email].first == msg
  end

  test "Create Valid User with invalid password" do
    user_data = users(:invalid_password)
    user = User.create(parse_json(user_data))
    msg = I18n.t('validate.user.password')
    assert user.errors.messages[:password].first == msg
  end

  test 'should send email signup when Create Valid User' do
    mock = MiniTest::Mock.new
    mock.expect(:send_signup_email, true)

    user = build(:valid_user)
    user.stub(:send_signup_email, lambda {
      mock.send_signup_email
    }) do
      user.save
    end

    assert mock.verify
  end

  test 'should send forget password to user' do
    mock = MiniTest::Mock.new
    mock.expect(:send_forget_password_email, true)

    user = build(:valid_user)
    user.stub(:send_forget_password_email, lambda {
      mock.send_forget_password_email
    }) do
      user.send_forget_password_email
    end

    assert mock.verify
  end
end
