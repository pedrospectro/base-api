require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test "Create Role Without name" do
    role = Role.new(
      name: nil,
      description: 'description'
    )
    assert_not role.save
  end

  test "Create Role Without description" do
    role = Role.new(
      name: 'name',
      description: ''
    )
    assert_not role.save
  end

  test "Create Valid Role" do
    role = Role.new(
      name: 'name',
      description: 'description'
    )
    assert role.save
  end
end
