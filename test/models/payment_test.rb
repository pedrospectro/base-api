require 'test_helper'

class PaymentTest < ActionDispatch::IntegrationTest
  test "Create Payment without Pedido" do
    payment = Payment.create(provider: 'pagseguro')
    assert_not payment.save
  end

  test "Create Payment without fields" do
    pedido = User.first.pedidos.create(
      items: [
        Item.create(@default_item)
      ]
    )
    payment = pedido.create_payment

    assert payment.errors.messages[:provider].first.include?(
      I18n.t('msg.payment.provider_presence')
    )
    assert_not payment.save
  end

  test "Create Payment with Pedido" do
    user = User.first
    @new_item = {
      quantity: 2,
      unity_value: 100.5
    }
    pedido = user.pedidos.create(
      items: [
        Item.create(@new_item)
      ]
    )
    @item = pedido.items.first
    @payment = pedido.create_payment(
      provider: 'pagseguro'
    )
    assert @payment.save
    assert @payment.status == pedido.status
    assert @payment.pedido_id == pedido.id
    assert @item.pedido.id == pedido.id
    assert pedido.user_id == user.id
    assert @item.total_value == (@item.quantity * @item.unity_value)
    assert pedido.total_value == @item.total_value
  end

  # test "Create another payment must not be saved" do
  #   user = User.first
  #   pedido = user.pedidos.create(items: [Item.create(@default_item)])
  #   @item = pedido.items.first
  #   @payment = pedido.create_payment(
  #     provider: 'pagseguro'
  #   )
  #   assert @payment.save
  #   @another_payment = pedido.create_payment(
  #     provider: 'pagseguro'
  #   )
  #   assert_not @another_payment.save
  # end

  test 'should send messages on Create Payment with Pedido' do
    mock = MiniTest::Mock.new
    mock.expect(:send_checkout_message, true)
    payment = build(:new_valid_payment)
    payment.stub(:send_checkout_message, lambda {
      mock.send_checkout_message
    }) do
      payment.save
    end

    assert mock.verify
  end
end
