require 'test_helper'

class PedidoTest < ActiveSupport::TestCase
  test "Create Pedido Withouth User" do
    pedido = Pedido.new
    assert_not pedido.save
  end

  test "Create Pedido With user" do
    user = User.find(parse_json(users(:valid_user))["id"])
    item = Item.create(@default_item)
    pedido = user.pedidos.new(items: [item])
    assert pedido.save
    assert pedido.status == 'ready'
    assert pedido.items.count == 1
    assert pedido.total_value = item.total_value
  end

  test "Create Pedido Without Item" do
    user = User.find(parse_json(users(:valid_user))["id"])
    pedido = user.pedidos.new
    assert_not pedido.save
    assert pedido.errors.messages[:items].first.include?(
      I18n.t('validate.pedido.must_have_one_item_or_more')
    )
  end

  test "Assert pedido send messages on create" do
    mock = MiniTest::Mock.new
    mock.expect(:send_msg, true)
    pedido = build(
      :new_ready,
      user: User.first,
      items: [Item.create(@default_item)]
    )
    pedido.stub(:send_msg, lambda {
      mock.send_msg
    }) do
      pedido.save
    end

    assert mock.verify
  end
end
