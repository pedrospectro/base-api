require 'test_helper'

class RolesUserTest < ActiveSupport::TestCase
  test "Create RolesUser Without user" do
    role_user = RolesUser.new(
      user: nil,
      role: roles(:admin)
    )
    assert_not role_user.save
  end

  test "Create RolesUser Without Role" do
    role_user = RolesUser.new(
      user: users(:user_one),
      role: nil
    )
    assert_not role_user.save
  end

  test "Create RolesUser" do
    role_user = RolesUser.new(
      user: users(:user_one),
      role: roles(:admin)
    )
    assert role_user.save
  end
end
