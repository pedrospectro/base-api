require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test "Create Product without data" do
    product = Product.new
    assert_not product.save
  end

  test "Create Product with only name" do
    product = Product.new(
      name: "Product x"
    )
    assert_not product.save
  end

  test "Create Product with only name,category" do
    product = Product.new(
      name: "Product x",
      category: "Category Y"
    )
    assert_not product.save
  end

  test "Create Product with only name,category,value" do
    product = Product.new(
      name: "Product x",
      category: "Category Y",
      value: 100
    )
    assert_not product.save
  end

  test "Create Product with only name,category,value,warranty" do
    product = Product.new(
      name: "Product x",
      category: "Category Y",
      value: 100,
      warranty: "1 mes"
    )
    assert_not product.save
  end

  test "Create Product without user and all required data" do
    product = Product.new(
      name: "Product x",
      category: "Category Y",
      value: 100,
      warranty: "1 mes",
      quantity: 200
    )
    assert_not product.save
  end

  test "Create Product with user and all required data" do
    product = Product.new(
      name: "Product x",
      category: "Category Y",
      value: 100,
      warranty: "1 mes",
      quantity: 200,
      user_id: 1
    )
    assert product.save
  end
end
