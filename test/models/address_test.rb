require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  test "Create Address withouth postal code" do
    address = Address.new
    assert_not address.save
  end

  test "Create Address with postal code" do
    address = Address.new(
      postal_code: '80030010',
      country: 'Brasil',
      uf: 'PR',
      city: 'Curitiba',
      neigborhood: 'Centro',
      street: 'Luiz Leao'
    )
    assert address.save
  end
end
