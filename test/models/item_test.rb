require 'test_helper'

class ItemTest < ActionDispatch::IntegrationTest
  test "Create Item without Pedido" do
    item = Item.create
    assert_not item.save
  end

  test "Create Item without fields" do
    pedido = User.first.pedidos.create(items: [Item.create(@default_item)])
    item = pedido.items.create
    assert item.errors.messages[:quantity].first.include?(
      I18n.t('msg.item.quantity_presence')
    )
    assert item.errors.messages[:unity_value].first.include?(
      I18n.t('msg.item.unity_value_presence')
    )
    assert_not item.save
  end

  test "Create Item with Pedido" do
    user = create(:pedido_user)
    pedido = create(:valid_pedido, items: [Item.create(@default_item)])
    item = build(:valid_item)
    @item = pedido.items.create(
      quantity: item.quantity,
      unity_value: item.unity_value
    )
    assert @item.save
    assert @item.pedido.id == pedido.id
    assert pedido.user.id == user.id
    assert @item.total_value == (@item.quantity * @item.unity_value)
  end
end
