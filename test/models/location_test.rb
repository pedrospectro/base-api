require 'test_helper'

class LocationTest < ActiveSupport::TestCase
  test "Create Location Without Address" do
    location = Location.new
    assert_not location.save
  end

  test "Create Location With Address" do
    location = Address.find(addresses(:new_address).id).locations.new
    assert location.save
  end
end
