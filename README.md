# README

* Developer:

    Name: Pedro Henrique Cavalcante S
    email: pedroccavalcante@gmail.com

* API DOC:

    https://documenter.getpostman.com/view/710295/base-api/7Loe2PE

* Ruby version

   Ruby - 2.3.3
   Rails - 5.1.4

* System dependencies

   Gemfile list and mysql server

* RuboCop

    This Project uses rubocop to help you about good codings

* Configuration

    You need only to install dependencies with 'bundle install'. Next step is to ensure that the mysql server is running ok. 

    ----------------------Important----------------------
    *** You must ensure that the file config/database.yml  is ok with your mysql user and password. Please fix it before you complete tanother readme list tasks
    -----------------------------------------------------

* Services (job queues, cache servers, search engines, etc.)

    1) GetAddressByCepJob - Use a Brazilian correios api to get info about a place, passing only the cep address as
    an argument

    2) GetLocationByAddressJob - Use the g maps api to get locations info.

* Circle CI

    This app uses circleci engine to ensure that the dockerizer application, environments, db dependencies and db configs will be ok.

    The circle ci runs all tests to ensure that the deploy will be ok.

    After commit on a branch the branch will be analyzed on circle ci app. The circleci will check whether all dependencies and tests will be successful. 

* Deployment instruction with docker

    1) Comando para preparar imagens e containers

    docker-compose up -d mysql && docker-compose -f docker-compose.test.yml build tests &&  docker-compose -f docker-compose.test.yml run --rm tests rake db:create db:migrate db:seed && docker-compose build app && docker-compose run --rm app rake db:create db:migrate db:seed

    2) Rodando os testes

    docker-compose -f docker-compose.test.yml up tests

    3) Executando o deploy da aplicação

    docker-compose up app

    4) Produção

    docker-compose -f docker-compose.prod.yml build app

    docker-compose -f docker-compose.prod.yml run --rm app rake db:create db:migrate db:seed

    docker-compose -f docker-compose.prod.yml up app

    5) Parar Aplicação

    docker-compose down

    6) Removendo Containers

    docker stop $(docker ps -aq) && docker rm $(docker ps -aq)

    7) Removendo Imagens

    docker rmi $(docker images -q) -f
    Instruções para criar pull requests e code review

    https://confluence.atlassian.com/bitbucket/pull-requests-and-code-review-223220593.html https://www.atlassian.com/git/tutorials/learn-about-code-review-in-bitbucket-cloud